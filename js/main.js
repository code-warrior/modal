/*global window */

window.onload = function () {
    'use strict';

    var mask = document.getElementById('mask'),
        maskImage = document.getElementById('mask-image'),
        links = document.querySelectorAll('a'),
        isMaskDisplayed = false;

    /**
     * HIDE MASK
     */
    function hideMask() {
        mask.style.display = 'none';
        maskImage.style.display = 'none';
    }

    /**
     * TOGGLE MASK
     */
    function toggleMask() {
        if (isMaskDisplayed) {
            mask.style.display = 'none';
            mask.removeEventListener('click', hideMask, false);
        } else {
            mask.style.display = 'block';
            mask.addEventListener('click', hideMask, false);
        }
    }

    /**
     * SET PATH TO MASK IMAGE
     */
    function setPathToMaskImage(option) {
        switch (option) {
            case 0:
                maskImage.setAttribute('src', 'img/wallabies-600-600.jpg');

                break;

            case 1:
                maskImage.setAttribute('src', 'img/koala-600-600.jpg');

                break;

            case 2:
                maskImage.setAttribute('src', 'img/kookaburra-600-600.jpg');

                break;
        }
    }

    /**
     * TOGGLE IMAGE
     */
    function toggleImage(option) {
        if (!isMaskDisplayed) {
            maskImage.style.display = 'block';
            setPathToMaskImage(option);
        }
    }

    /**
     * TOGGLE MODAL
     */
    function toggleModal(option) {
        toggleMask();
        toggleImage(option);
    }

    links[0].addEventListener('click', function (evnt) {
        evnt.preventDefault();
        toggleModal(0);
    }, false);
    links[1].addEventListener('click', function (evnt) {
        evnt.preventDefault();
        toggleModal(1);
    }, false);
    links[2].addEventListener('click', function (evnt) {
        evnt.preventDefault();
        toggleModal(2);
    }, false);
};
